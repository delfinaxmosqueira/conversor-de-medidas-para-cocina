# Conversor de medidas para cocina

El proyecto se basa en un conversor gramos-tazas-mililitros para distintos tipos de ingredientes de cocina. 

Primer paso: Seleccione el ingrediente.

Segundo paso: Seleccione el tipo de conversión dentro de las opciones.

Tercer paso: Ingrese su medida.

Cuarto paso: Obtenga la conversión.

Quinto paso: Elija si continuar hacia el primer paso o terminar el programa.
